$(document).ready(function() {

	$('.js-main').waypoint(function(direction){
		if(direction=="down"){
			$('nav').addClass('sticky-nav');
		}else{
			$('nav').removeClass('sticky-nav');
		}
	},{
		offset: '50px;'
	});

	$('#panel-info').waypoint(function(direction){
		if(direction=="down"){
			var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
			$('#num-1').animateNumber({
				number: 3020,
				numberStep: comma_separator_number_step
			});
			$('#num-2').animateNumber({
				number: 234873,
				numberStep: comma_separator_number_step
			});
			$('#num-3').animateNumber({
				number: 4670,
				numberStep: comma_separator_number_step
			});
			$('#num-4').animateNumber({
				number: 1234,
				numberStep: comma_separator_number_step
			});
		}
	},{
		offset: '320px;'
	});

	$('.bxslider').bxSlider({
		minSlides: 1,
		maxSlides: 1,
		mode: 'fade',
		auto: true,
		pager: false
	});	

	$('#filter a').on('click', function() {
		$('#filter li.current').removeClass('current');
		$(this).parent().addClass('current');

		var category = $(this).text().toLowerCase().replace(' ', '-');
		console.log(category);


		if(category === 'all'){
			$('#portafolio li:hidden').fadeIn('slow').removeClass('hidden');
		}else{
			$('#portafolio li').each(function(){
				if(!$(this).hasClass(category)){
					$(this).hide().addClass('hidden');
				}else{
					$(this).fadeIn('slow').removeClass('hidden');
				}
			});
		}
		return false;
	});

	$('.js-scroll-home').on('click', function() {
		$('html, body').animate({scrollTop: 0}, 800);

		return false;
	});

	/*CSS Tricks Smooth Scrolling*/
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({ scrollTop: target.offset().top }, 1000);
				return false;
				}
			}
		});
	});	

	$('.js-mobile').on('click', function() {
		var nav = $('.main-nav');
		var menu = $('.mobile-menu');

		nav.slideToggle(200);
		
		if(menu.hasClass('icon-close')){
			menu.removeClass('icon-close');
			menu.addClass('icon-menu');
		}else{
			menu.removeClass('icon-menu');
			menu.addClass('icon-close');
		}

		return false;
	});

});


