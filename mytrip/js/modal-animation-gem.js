$(document).ready(function() {
  	var $main_wrap = $('.main'),
	$form_modal = $('.cd-user-modal'),
	$form_login = $form_modal.find('#cd-login'),
	$login_link = $('.cd-signin');

	//open modal
	$login_link.on('click', function(event){
		event.preventDefault();
		//show modal layer
		$form_modal.addClass('is-visible');	
		$form_login.addClass('is-selected');
	});

	//close modal
	$('.cd-user-modal').on('click', function(event){
		if( $(event.target).is($form_modal) ) {
			$form_modal.removeClass('is-visible');
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$form_modal.removeClass('is-visible');
	    }
    });		

    $(window).scroll(function(event){
    	var y = $(this).scrollTop();

    	if(y>=780){
    		$('#benefits-one').addClass('zoomIn');
    	}
    	if(y>=1180){
    		$('#benefits-two').addClass('zoomIn');
    	}    	
    	if(y>=1540){
    		$('#benefits-three').addClass('zoomIn');
    	}       
    	if(y>=2930){   		
    		$('#title-call-to-action').addClass('fadeInDown');
    	}   

    });	

});  	


