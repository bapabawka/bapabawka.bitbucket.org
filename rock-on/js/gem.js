$(document).ready(function(){

	var arrData = new Array();
	var current = 0;
   
	var request = $.ajax({
		type: "GET",
		url: "http://hardrocklv.me/apiv1/upcomingevents.xml?venueids=1105345,3101438625&picgroupid=701",
		dataType: "xml",
		success: function(xml){
			$(xml).find('event').each(function(){
				var eName = $(this).find('name').text();
				var eDate = $(this).find('eventdate').text();
				var eTime = $(this).find('startime').text();
				
				var eTickets;
				if($(this).find('hastickets').text()=== '1'){
					eTickets = "Yes";
				}else{
					eTickets = "No";
				}
				
				var eDesc = $(this).find('pubdescr').text();
				var eImage = $(this).find('picfolder').eq(0).text()+'/300SC300/'+$(this).find('picfile').eq(0).text();
				var data = 'Event Name: ' + eName + ', Event Date:' + eDate + ', Event Time: ' + eTime + ', Event Has Tickets: ' + eTickets + ', Event Description: ' + eDesc;

				var html = [
					'<div id="',
						current,
						'"><figure><img src="',
							eImage,
						'"/><figcaption><h3>',
						eName,
						'</h3><p>',
						moment(eDate).format('MMM DD'),
						'</p></figcaption>',
					'</figure></div>'
				];				
				
				$('.slides').removeClass('loading');
				$(html.join('')).appendTo(".slides");				
				arrData.push(data);
				current++;
			});
		},
		error: function() {
			alert("An error occurred while processing XML file.");
		}
	});
	
	// Slick Slider
    request.done(function() {
		$('.slides').slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 5,
		  responsive: [
			{
			  breakpoint: 1280,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 4,
			  }
			},		  
			{
			  breakpoint: 960,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		  ]
		});
    });	

	$('.slides').on('click', 'figcaption', function() {
		alert( arrData[$(this).closest('div').attr('id')] );
	});
	
});