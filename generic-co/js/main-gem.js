$(document).ready(function() {	
	$('.flexslider').flexslider({
		animation: "fade",
		controlNav: true,
		directionNav: false
	});

  	var	$form_contact = $('.contact-form'),
	$input_name = $form_contact.find('#contact-name'),
	$input_tel = $form_contact.find('#contact-tel'),
	$input_email = $form_contact.find('#contact-email');

    $input_name.keyup(function() {
		//Test regular expression
		//	Accept 4-15 characters without any special characters
		var user = $('#contact-name').val();
		var filter = /^[a-z0-9_-]{4,15}$/;
		if(filter.test(user)){
			$(this).removeClass('input-error').next('span').removeClass('is-visible');
			return true;
		}
		else{
			$(this).addClass('input-error').next('span').addClass('is-visible');
			return false;
		}
    });

    $input_tel.keyup(function() {
		//Test regular expression
		//	Must be at least 8 characters
		//  At least 1 number, 1 lowercase, 1 uppercase letter
		//  At least 1 special character from @#$%&
		var password = $('#contact-tel').val();
		var filter = /^[0-9]+$/;
		if(filter.test(password)){
			$(this).removeClass('input-error').next('span').removeClass('is-visible');
			return true;
		}
		else{
			$(this).addClass('input-error').next('span').addClass('is-visible');
			return false;
		} 	
	});   	

    $input_email.keyup(function() {
		//Test regular expression
		//	Accept valid email address
		var email = $('#contact-email').val();
		var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		if(filter.test(email)){
			$(this).removeClass('input-error').next('span').removeClass('is-visible');
			return true;
		}
		else{
			$(this).addClass('input-error').next('span').addClass('is-visible');
			return false;
		}
    });

	$form_contact.find('input[type="submit"]').on('click', function(event){
		event.preventDefault();
	});
				
});  	


