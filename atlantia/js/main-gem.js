$(document).ready(function() {	
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-aero',
		radioClass: 'iradio_square-aero',
		increaseArea: '20%' // optional
	});
});  	
$("#showLeftPush").click(function() {
	$(this).toggleClass("active");
	$(".wrap-main").toggleClass("at-spmenu-push-toright");
	$("#at-spmenu").toggleClass("at-spmenu-open");
	$(".at-grid-cards").children(".at-card-morph--js").toggleClass("col-lg-3");
	$(".at-grid-cards").children(".at-card-morph--js").toggleClass("col-lg-4");
});	